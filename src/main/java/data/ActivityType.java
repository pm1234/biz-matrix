package data;

/**
 * Created by Piotr1 on 16.05.2017.
 */
public enum ActivityType {
    PRIVATE, BUSINESS, UNKNOWN
}

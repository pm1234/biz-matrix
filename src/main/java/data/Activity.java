package data;

import java.util.Calendar;
/**
 * Created by Piotr1 on 16.05.2017.
 */
public class Activity {
    private Calendar date;
    private ActivityType activityType;

    public Activity() {    }

    public Calendar getDate() {
        return date;
    }

    public Activity setDate(Calendar date) {
        this.date = date;
        return this;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public Activity setActivityType(ActivityType activityType) {
        this.activityType = activityType;
        return this;
    }
}

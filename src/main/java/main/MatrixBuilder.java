package main;

import data.Activity;
import data.ActivityType;

import java.io.*;
import java.util.*;

/**
 * Created by Piotr1 on 16.05.2017.
 */
public class MatrixBuilder {
    private File directory;
    private Map<String, List<Activity>[][]> usersActivities;
    private Map<String, double[][]> probabilitiesMatrices;
    private Map<String, String[][]> businessActivitiesMatrices;

    public MatrixBuilder(File directory) {
        this.directory = directory;
        usersActivities = new HashMap<>();
        probabilitiesMatrices = new HashMap<>();
        businessActivitiesMatrices = new HashMap<>();
        listUsers();
        for (String user : usersActivities.keySet()) {
            for (int i=0; i<usersActivities.get(user).length; i++) {
                for (int j=0; j<usersActivities.get(user)[i].length; j++) {
                    usersActivities.get(user)[i][j] = new LinkedList();
                }
            }
        }
    }

    private void listUsers() {
        File [] csvFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".csv");
            }
        });
        String line;
        assert csvFiles != null;
        System.out.println(Main.APP_NAME+csvFiles.length+" CSV files found.");
        for (File source : csvFiles) {
            try (BufferedReader br = new BufferedReader(new FileReader(source))) {
                line = br.readLine();
                if (line.equals("DateTime,deviceId,contactType,duration,callLogEntryId,type,number")) { //calls file
                    String [] dataExample;
                    while ( (line = br.readLine()) != null) {
                        dataExample = line.split(",");
                        usersActivities.put(dataExample[1],new LinkedList[7][24]);
                        probabilitiesMatrices.put(dataExample[1],new double[7][24]);
                        businessActivitiesMatrices.put(dataExample[1],new String[7][24]);
                    }
                    System.out.println(Main.APP_NAME+"Number of distinct users in "+source.getName()+" found: "+usersActivities.keySet().size());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void clusterActivities() {
        System.out.println(Main.APP_NAME+"Clustering activities...");
        File [] csvFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".csv");
            }
        });
        assert csvFiles != null;
        for (File source : csvFiles) {
            String line;
            String [] dataExample;
            int lineIndex = 1;
            try (BufferedReader br = new BufferedReader(new FileReader(source))) {
                Calendar.Builder tempCal = new Calendar.Builder();
                ActivityType tempType = ActivityType.UNKNOWN;
                line = br.readLine();
                if (line.equals("DateTime,deviceId,contactType,duration,callLogEntryId,type,number")) {
                    System.out.println(Main.APP_NAME+"Processing file: "+source.getName());
                    while ((line = br.readLine()) != null) {
                        lineIndex++;
                        dataExample = line.split(",");
                        //0-DateTime; 1-deviceId; 2-contactType; 3-duration; 4-callLogEntryId; 5-type; 6-number
                        //0 (sample: 2017-01-01T11:46:38.111Z) and 2 (P/B/?) are relevant
                        tempCal.setDate(Integer.parseInt(dataExample[0].substring(0, 4)),
                                Integer.parseInt(dataExample[0].substring(5, 7)) - 1,
                                Integer.parseInt(dataExample[0].substring(8, 10)));
                        tempCal.setTimeOfDay(Integer.parseInt(dataExample[0].substring(11, 13)), 0, 0);

                        switch (dataExample[2]) {
                            case "P":
                                tempType = ActivityType.PRIVATE;
                                break;
                            case "B":
                                tempType = ActivityType.BUSINESS;
                                break;
                            case "?":
                                tempType = ActivityType.UNKNOWN;
                                break;
                            default:
                                System.err.println(Main.APP_NAME + source.getName() + ": Unknown symbol '" + dataExample[2] + "' in line " + lineIndex);
                        }
                        Activity newActivity = new Activity();
                        newActivity.setActivityType(tempType).setDate(tempCal.build());
                        usersActivities.get(dataExample[1])[newActivity.getDate().get(Calendar.DAY_OF_WEEK)-1][newActivity.getDate().get(Calendar.HOUR_OF_DAY)].add(newActivity);
                    }
                } else if (line.equals("date,accountId")) { //EMAILS FILE HEADER
                    //do nothing so far
//                    while ( (line = br.readLine()) != null) {
//                        lineIndex++;
//                        dataExample = line.split(",");
//                        //0-date; 1-accountId
//                        //0 (sample: 2017-01-01T11:46:38.111Z) is relevant
//                        //(activity type is business as default)
//                        tempCal.setDate(Integer.parseInt(dataExample[0].substring(0,4)),
//                                Integer.parseInt(dataExample[0].substring(5,7)),
//                                Integer.parseInt(dataExample[0].substring(8,10)));
//                        tempCal.setTimeOfDay(Integer.parseInt(dataExample[0].substring(11,13)),0,0);
//                        Activity newActivity = new Activity();
//                        newActivity.setActivityType(ActivityType.BUSINESS).setDate(tempCal.build());
//                        activitiesMatrix[newActivity.getDate().get(Calendar.DAY_OF_WEEK)-1][newActivity.getDate().get(Calendar.HOUR_OF_DAY)].add(newActivity);
//                    }
                } else {
                    System.err.println("Unknown file header: "+line);
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void computeProbabilities() {
        System.out.println(Main.APP_NAME+"Computing probabilities...");
        assert usersActivities != null;
        double [][] currentProbArray;
        String [][] currentRatioArray;
        for (String user : usersActivities.keySet()) {
            for (int i=0; i<usersActivities.get(user).length; i++) {
                for (int j = 0; j < usersActivities.get(user)[i].length; j++) {
                    int activitiesNumber = usersActivities.get(user)[i][j].size();
                    int businessActivitiesNumber = 0;
                    for (Activity a : usersActivities.get(user)[i][j]) {
                        if (a.getActivityType() == ActivityType.BUSINESS)
                            businessActivitiesNumber++;
                    }
                    probabilitiesMatrices.get(user)[i][j] = (activitiesNumber > 0 ? (double) businessActivitiesNumber / activitiesNumber : 0);
                    businessActivitiesMatrices.get(user)[i][j] = String.format("%d/%d", businessActivitiesNumber, activitiesNumber);
                }
            }
        }
    }

    public String probabilitiesToString() {
        final String [] weekDays = {"Su","Mo","Tu","We","Th","Fr","Sa"};
        StringBuilder sb = new StringBuilder();
        sb.append("* * * * * PROBABILITIES MATRICES * * * * *\n");
        for (String user : usersActivities.keySet()) {
            sb.append("\nUser ").append(user).append(":\n");
            sb.append("\n\t");
            for (int i=0; i<23;i++)
                sb.append(String.format("%9d",i)).append("\t");
            sb.append(String.format("%9s\n",23));
            for (int i=1; i<probabilitiesMatrices.get(user).length; i++) {
                sb.append(weekDays[i]).append("\t");
                for (int j = 0; j < probabilitiesMatrices.get(user)[i].length; j++) {
                    sb.append(String.format("%9.1f",probabilitiesMatrices.get(user)[i][j]*100)).append("\t");
                }
                sb.append("\n");
            }
            sb.append(weekDays[0]).append("\t");
            for (int j = 0; j < probabilitiesMatrices.get(user)[0].length; j++) {
                sb.append(String.format("%9.1f",probabilitiesMatrices.get(user)[0][j]*100)).append("\t");
            }
            sb.append("\n");
        }

        return sb.toString()+"\n----------------------------\n";
    }

    public String activitiesCountToString() {
        final String [] weekDays = {"Su","Mo","Tu","We","Th","Fr","Sa"};
        StringBuilder sb = new StringBuilder();
        sb.append("* * * * * ACTIVITIES MATRICES * * * * *\n");
        for (String user : usersActivities.keySet()) {
            sb.append("\nUser ").append(user).append(":\n");
            sb.append("\t");
            for (int i=0; i<23;i++)
                sb.append(String.format("%9d",i)).append("\t");
            sb.append(String.format("%9s\n",23));
            for (int i=1; i<businessActivitiesMatrices.get(user).length; i++) {
                sb.append(weekDays[i]).append("\t");
                for (int j = 0; j < businessActivitiesMatrices.get(user)[i].length; j++) {
                    sb.append(String.format("%9s",businessActivitiesMatrices.get(user)[i][j])).append("\t");
                }
                sb.append("\n");
            }
            sb.append(weekDays[0]).append("\t");
            for (int j = 0; j < businessActivitiesMatrices.get(user)[0].length; j++) {
                sb.append(String.format("%9s",businessActivitiesMatrices.get(user)[0][j])).append("\t");
            }
            sb.append("\n");
        }

        return sb.toString()+"\n";
    }

    public boolean buildMatrices() {
        try {
            System.out.println(Main.APP_NAME+"Scanning directory: "+directory.getCanonicalPath());
            clusterActivities();
            computeProbabilities();
            return true;
        } catch (Exception e) {
            System.err.println("Error occured while building matrices.\nDetails: ");
            e.printStackTrace();
            return false;
        }
    }
}

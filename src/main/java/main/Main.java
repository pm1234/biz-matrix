package main;
import java.io.*;

/**
 * Created by Piotr1 on 13.05.2017.
 */
public class Main {
    public static final String APP_NAME = "BIZMATRIX: ";
    /**
     * Main matrix (rows - week days, cols - time slots). Every field contains all activities assigned to given day and hour.
     */
    public static void main (String[] args) {
        //args[0] (if present) -- path to a folder with CSV files
        if (args == null || args.length == 0) {
            System.out.println(APP_NAME+" options:");
            System.out.println("--path=path_to_folder_with_csv_files (mandatory; may indicate absolute of relative path)");
            System.out.println("--out_p=path_to_file_with_probabilities_matrices (optional; if not present, matrices will be displayed in the command window");
            System.out.println("--out_c=path_to_file_with_activities_count_matrices (optional; if not present, matrices will be displayed in the command window");
            System.exit(0);
        }
        if (!args[0].startsWith("--path=")) {
            System.err.println(APP_NAME+"Error - no --path option present as the first parameter!");
            System.exit(-1);
        }
        String path = args[0].substring(args[0].indexOf('=')+1);
        String probPath = "", countPath = "";
        for (String a : args) {
            if (a.startsWith("--out_p=")) probPath = a.substring(a.indexOf('=')+1);
            else if (a.startsWith("--out_c=")) countPath = a.substring(a.indexOf('=')+1);
        }
        File directory = new File(path);

        /* if given path points to a directory*/
        if (directory.isDirectory()) {
            MatrixBuilder matrixBuilder = new MatrixBuilder(directory);
            boolean isSuccess = matrixBuilder.buildMatrices();
            if (isSuccess) { //computational process ended successfully
                if (!probPath.equals("")) {
                    try {
                        BufferedWriter bw = new BufferedWriter(new FileWriter(probPath));
                        bw.write(matrixBuilder.probabilitiesToString());
                        bw.close();
                    } catch (IOException e) {e.printStackTrace();}
                }
                else System.out.println(matrixBuilder.probabilitiesToString());

                if (!countPath.equals("")) {
                    try {
                        BufferedWriter bw = new BufferedWriter(new FileWriter(countPath,countPath.equals(probPath)));
                        bw.write(matrixBuilder.activitiesCountToString());
                        bw.close();
                    } catch (IOException e) {e.printStackTrace();}
                }
                else System.out.println(matrixBuilder.activitiesCountToString());
            }
        }

        /* if given path doesn't point to a directory*/
        else {
            System.err.println(APP_NAME+"Error: Path "+directory.getAbsolutePath()+" is not a directory!");
        }
    }
}
